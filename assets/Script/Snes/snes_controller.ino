#include <Snes.h>

Snes snes;

void setup()
{
  snes.init();
}
    
void loop()
{
  //Pour ne récupérer que les données
  //int donnee = snes.data();
  //Serial.println(donnee);
  //delay(250);

  //Pour récupérer les données et les afficher
  snes.print();
}
