#include <SPI.h>
#include <RFID.h>

//Création d'un objet monModuleRFID
RFID monModuleRFID(10,9);

//Tableau de stockage de l'UID
int UID[5];

void setup()
{
  //Initialisation des librairies
  Serial.begin(9600);
  SPI.begin();
  monModuleRFID.init();
}

void loop()
{
    //Si un badge est detecté
    if (monModuleRFID.isCard()) 
    {  
          //Si on lit un badge
          if (monModuleRFID.readCardSerial()) 
          {        
            Serial.print("UID lu : ");
            for(int i=0;i<=4;i++)
            {
              UID[i]=monModuleRFID.serNum[i];     //Stockage de l'UID
              Serial.print(UID[i],DEC);           //Affichage de l'UID
              if(i!=4)
                Serial.print(".");
            }
            Serial.println("");
          }          
          monModuleRFID.halt();
    }
    delay(500);    
}
