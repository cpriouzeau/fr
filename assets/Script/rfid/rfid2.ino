#include <SPI.h>
#include <RFID.h>
#include <Servo.h>

//Ports utilisés
const char LED_BLEU = PB5;
const char LED_ROUGE = PB1;
const char LED_VERTE = PB0;

//Création des objets
RFID monModuleRFID(10,9);
Servo moteur;

//Variables globales
int UID[5]={};
int MASTERKEY[5]={};
int compteur = 0;

void setup()
{
  //Initialisation des librairies
  Serial.begin(9600);
  SPI.begin();
  monModuleRFID.init();  

  //Initialisation des ports
  moteur.attach(6);
  pinMode(LED_BLEU, OUTPUT);
  pinMode(LED_ROUGE, OUTPUT);
  pinMode(LED_VERTE, OUTPUT);
  digitalWrite(LED_BLEU, HIGH);
  digitalWrite(LED_ROUGE, LOW);
  digitalWrite(LED_VERTE, LOW);
}

void loop()
{
  //A la lecture d'un badge
  if (monModuleRFID.isCard()) 
  {  
    //Enregistrement de l'UID d'un badge 
    if (monModuleRFID.readCardSerial() && compteur == 0)
    {
      Serial.print("----------------------------------------\n| UID enregistré : ");
      for(int i=0;i<=4;i++)
      {
        MASTERKEY[i]=monModuleRFID.serNum[i];     //Recupere les 5 valeurs de l'UID
        Serial.print(MASTERKEY[i],DEC);
        if(i!=4)
          Serial.print(".");
      }
      Serial.println(" |\n----------------------------------------");
      compteur++;
      digitalWrite(LED_BLEU, LOW);
    }

    //Affichage de l'UID dans le moniteur série
    if (monModuleRFID.readCardSerial() && compteur != 0) 
    {        
      Serial.print("UID lu : ");
      for(int i=0;i<=4;i++)
      {
        UID[i]=monModuleRFID.serNum[i];
        Serial.print(UID[i],DEC);
        if(i!=4)
          Serial.print(".");
      }
      Serial.println("");

      //Vérification du badge et mise en marche du moteur
      if (UID[0] == MASTERKEY[0] && UID[1] == MASTERKEY[1] && UID[2] == MASTERKEY[2] && UID[3] == MASTERKEY[3] && UID[4] == MASTERKEY[4])
      {
        digitalWrite(LED_VERTE, HIGH);
        moteur.write(180);
        Serial.println("Porte ouverte");
        delay(3000);
        moteur.write(90);
        Serial.println("Porte fermée");
        digitalWrite(LED_VERTE, LOW);
      }
      else
      {
        digitalWrite(LED_ROUGE, HIGH);
        delay(1000);
        digitalWrite(LED_ROUGE, LOW);
      }          
      monModuleRFID.halt();
    }
  }   
}
