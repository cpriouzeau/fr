from us100 import US100
from pyb import Pin, Timer
import time

sonar=US100()

BUZZER = Pin('A3') # Pin A3 (shield Grove et connecteur Arduino) cablée sur Pin PA0 (microcontroleur)
tim = Timer(2, freq=440)	# TIM2 channel 1 cablé sur A3
ch = tim.channel(1, Timer.PWM, pin=BUZZER)	#Mode PWM sur sur A3

# Initialisation du flag d'interruption
i = 0

# Initialisation du bouton poussoir
sw=pyb.Switch()

# Fonction d'interruption du bouton poussoir
def appuie():
	global i
	pyb.LED(1).toggle()
	if i==0:
		i = 1
	else:
		i = 0

sw.callback(appuie)

# Fonction qui fait sonner le buzzer
def buzz(temps):
	ch.pulse_width_percent(20) #DutyCycle de 20%
	time.sleep_ms(50)
	ch.pulse_width_percent(0)
	time.sleep_ms(temps)
	

while 1: # Boucle infinie qui enclenche le radar si il y'a eu appuie du bouton, l'éteint si appuie à nouveau
	if i == 1:
		d = sonar.distance_mm()/10
		print(d)
		
		# pas de son
		if d > 75:
			ch.pulse_width_percent(0)
			
		# son continu
		elif d < 5 and d > 0:
			buzz(0)
			
		# son évoluant en fonction de la distance
		else:
			t = int(d*5)
			buzz(t)
	# eteint le buzzer
	ch.pulse_width_percent(0)
	time.sleep_ms(5)
	
	
	

