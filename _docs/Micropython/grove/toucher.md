---
title: Interrupteur tactile
description: Mise en oeuvre d'un interrupteur tactile avec MicroPython
---

# Interrupteur tactile

Ce tutoriel explique comment mettre en oeuvre un interrupteur tactile avec MicroPython.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un capteur tactile Grove

**L'interrupteur tactile Grove (Touch sensor) :**

<div align="center">
<img alt="Grove touch sensor" src="images/grove_touch_sensor.jpg" width="300px">
</div>

On trouve bien sûr d'autres implémentations matérielle du capteur tacile que celle de Seeed Studio. Voici par exemple une version qui doit être câblée "à la main". Le capteur ne possède que 3 broches qu'il faut connecter à GND, VCC (alimentation, câbles rouge et noir) et D4 (signal, câble jaune).

<div align="center">
<img alt="Câblage interrupteur tactile" src="images/capteur_tactile.png" width="400px">
</div>

Il se comporte comme un interrupteur ; il sera dans l’état *ON* s’il y a contact et *OFF* dans le cas contraire.

**Le code MicroPython :**
```python
# Objet du script : Mise en oeuvre d'un capteur tactile

import pyb
from pyb import Pin
import time # Bibliothèque de temporisation

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)

while True :
	time.sleep_ms(500) # Temporisation de 500 millisecondes

	if(p_in.value() == 1): # Si on touche le capteur
		print("ON")
	else: # Autrement
		print("OFF")
```
