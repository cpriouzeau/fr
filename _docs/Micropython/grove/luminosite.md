---
title: Capteur de luminosité
description: Mise en oeuvre du capteur de luminosité Grove avec MicroPython
---

# Capteur de luminosité

Ce tutoriel explique comment mettre en oeuvre un capteur de luminosité analogique Grove avec MicroPython.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un capteur de luminosité Grove

**Le capteur de luminosité :**

<div align="center">
<img alt="Grove light sensor" src="images/capteur-de-lumiere.png" width="200px">
</div>

Ce capteur utilise une photorésistance afin de mesurer l'intensité lumineuse de son environnement. La valeur de cette photorésistance diminue lorsqu'elle est éclairée. Cet exemple est une nouvelle illustration, directe, de la conversion d'un signal en tension variable dans le temps (la réponse de la photodiode) en un signal numérique, via un ADC.

Connectez le capteur sur la **broche A1**.

**Le code MicroPython :**

```python
# Objet du script :
# Lecture et numérisation du signal d'un capteur de luminosité (photodiode).

import pyb
from pyb import Pin, ADC
import time

adc = ADC(Pin('A1'))
while True:
	# Numérise la valeur lue, produit un résultat variable dans le temps dans l'intervalle [0 ; 4095]
	print(adc.read())
	# Pause pendant 0.5s
	time.sleep(0.5)
```
Une valeur sera affichée dans le terminal série de l'USB user toutes les 0.5s.

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
