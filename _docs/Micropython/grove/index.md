---
title: Tutoriels avec des modules Grove et autres
description: Tutoriels avec des modules Grove et autres
---

# Tutoriels avec des modules Grove et autres

**L'ensemble des codes sources sont disponibles dans la [zone de téléchargement](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement).**<br>

## Démarrage

Pour la plupart de ces tutoriels, vous devrez disposer de la carte d’extension Grove (Grove Base Shield en anglais).
C'est une carte au format Arduino qui vient se connecter sur la NUCLEO-WB55 et permet de brancher aisément des capteurs et des actionneurs digitaux, analogiques, UART et I2C suivant la connectique [Grove](http://wiki.seeedstudio.com/Grove_System/).

<div align="center">
<img alt="Grove base shield pour Arduino" src="images/grove_base_shield.jpg" width="550px">
</div>

Elle offre :
* Six connecteurs D2, D3 ... pour des périphériques digitaux (0 ou 1) ;
* Quatre connecteurs A0, A1, ... pour des périphériques qui envoient un signal analogique en entrée (entre 0 et +3.3V ou 0 et +5V selon la position du petit interrupteur qui l'équipe) ;
* Quatre connecteurs pour des périphériques dialoguant avec le protocle I2C ;
* Un connecteur pour un port série (UART) ;
* Un commutateur 3.3V / 5V qui permet de sélectionner la tension d'alimentation des modules que vous connecterez sur ses fiches.

## Liste des tutoriels

Vous trouverez ici quelques tutoriels essentiellement [pour des modules au format Grove de la société Seeed Studio](http://wiki.seeedstudio.com/Grove_System/). 
Ils se transposeront facilement à des modules d'autres fabricants pour peu qu'ils utilisent les mêmes protocoles et bus. 
**Prenez garde cependant aux tensions d'alimentation des modules !** Certains nécessitent du 5V, d'autres fonctionnent aussi bien avec du 5V et du 3.3V, d'autres enfin fonctionnent exclusivement en 3.3V et seront endommagés par du 5V. **Lisez bien leurs fiches techniques avant de les brancher**.

Nous aborderons et expliquerons au gré des tutoriels des notions de programmation embarquée et d'architecture des microcontrôleurs qui vous seront indispensables pour progresser. Lorsque vous rencontrerez un acronyme que vous ne connaissez pas, n'hésitez pas à consulter le [glossaire](_docs/Micropython/glossaire.md).

</br>

**1. Afficheurs**

| | |
|-|-|
|**Tutoriel**|**Connectique / protocole**|
[Afficheur 4x7-segments TM1637](tm1637)|I2C|
[Afficheur 8x7-segments TM1638](tm1638)|I2C|
[Afficheur LCD RGB 16 caractères x 2 lignes](lcd_16x2)|I2C|
[Afficheur OLED 1308](oled_1308)|I2C|

<br>

**2. Capteurs**

| | |
|-|-|
|**Tutoriel**|**Connectique/protocole**|
[Adaptateur Nintendo Nunchuk](nunchuk)|I2C|
[Capteur de CO2 SCD30](scd30)|I2C|
[Capteur de pression, température et humidité BME280](bme280)|I2C|
[Capteur d'inclinaison](inclinaison)|digital|
[Capteur de luminosité](luminosite)|ADC|
[Détecteur de mouvement PIR](mouvement)|digital|
[Interrupteur tactile](toucher)|digital|
[Joystick](joystick)|ADC|
[Manette Nintendo SNES](snes)|SPI|
[Module GPS](gps)|UART|
[Potentiomètre](potentiometre)|ADC|
[Sonomètre](bruit)|ADC|
[Sonar à ultrasons](distance_ultrason)|UART|
[Sonde étanche de température DS1820](ds1820)|One-Wire|

<br>

**3. Enregistrement de données**

| | |
|-|-|
|**Tutoriel**|**Connectique/protocole**|
[Badges RFID](rfid)|SPI|
[Module carte SD](sd_card_module)|SPI|

<br>

**4. Actuateurs**

| | |
|-|-|
|**Tutoriel**|**Connectique/protocole**|
[Buzzer](buzzer)|PWM|
[LED infrarouge](del_ir)|digital|
[Servomoteur (bientôt)](servo)|PWM| 

<br>

**5. Autres**

| | |
|-|-|
|**Tutoriel**|**Connectique/protocole**|
[Alarme de mouvement](alarme)|PWM|
[Faire clignoter plusieurs LED](blink_many)|digital|
[Horloge temps-réel](rtc)|Bus interne au STM32WB55|
[Variateur de lumière](variateur_lumiere)|PWM|

> Crédit images : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/) et [Simple Circuit](https://simple-circuit.com/arduino-led-blink-seeed-studio-grove-led-socket-kit/)
