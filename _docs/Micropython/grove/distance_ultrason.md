---
title: Sonar à ultrasons
description: Mise en oeuvre du capteur de distance par ultrason US100 Grove en MicroPython
---

# Sonar à ultrasons

Ce tutoriel explique comment mettre en oeuvre le capteur de distance par ultrason US100 Grove en MicroPython.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un capteur à ultrasons US100 Grove

**Le capteur de distance à ultrasons :**

<div align="center">
<img alt="Grove - GPS" src="images/grove-ultrason.jpg" width="400px">
</div>

**Description :**

Le capteur US100 émet des ultrasons et calcule le temps qu'ils mettent à revenir vers lui, ce qui lui permet de mesurer la distance le séparant d'un objet, comprise entre 2 cm et 4,5 m.

Il communique avec la carte par le biais de la liaison série (UART) à une fréquence de 9600 bauds.
L'UART2 ou LPUART est câblée sur les pins PA2 et PA3 de la STM32WB55 qui correspondent au port « UART » du shield Grove.

**Montage :**

Pour faire communiquer le capteur avec la carte il faut connecter la pin Echo/Rx du capteur à la pin Rx du shield Grove et la pin Trig/Tx à la pin Dx du shield. Attention : il faut bien veiller à ce que les pins Dx/Tx et Rx soient bien croisées entre le capteur et la carte car ces lignes sont unidirectionnelles. Dx/Tx correspond à la transmission d'un message tandis que Rx correspond à sa réception. Le shield Grove réalise d'office ce croisement entre la carte et le capteur.

Il faut ensuite alimenter le capteur en reliant la pin VCC à la pin Vcc du shield et faire de même avec la pin GND. Certains capteurs ont 2 pins GND, il faudra donc relier les pins entre elles avant de les brancher sur le shield Grove ou alors tirer un fil supplémentaire pour connecter le deuxième GND à un GND quelconque de la carte STM ou du shield.

<div align="center">
<img alt="Schéma de câblage du capteur à ultrasons" src="images/US100_cablage.jpg" width="400px">
</div>

**Le code MicroPython :**

Pour simplifier le code nous utilisons une bibliothèque externe. La bibliothèque est disponible en suivant ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Ultrason/US100.py). Le fichier *main* est quant à lui disponible via ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Ultrason/main.py).

*Le listing complet :* 
```python
from us100 import US100
from pyb import Pin, Timer
import time

sonar=US100()

BUZZER = Pin('A3') # Pin A3 (shield Grove et connecteur Arduino) cablée sur Pin PA0 (microcontrôleur)
tim = Timer(2, freq=440)	# TIM2 channel 1 cablé sur A3
ch = tim.channel(1, Timer.PWM, pin=BUZZER)	#Mode PWM sur sur A3

# Initialisation du flag d'interruption
i = 0

# Initialisation du bouton poussoir
sw=pyb.Switch()

# Fonction d'interruption du bouton poussoir
def appuie():
	global i
	pyb.LED(1).toggle()
	if i==0:
		i = 1
	else:
		i = 0

sw.callback(appuie)

# Fonction qui fait sonner le buzzer
def buzz(temps):
	ch.pulse_width_percent(20) #Repport cyclique de 20%
	time.sleep_ms(50)
	ch.pulse_width_percent(0)
	time.sleep_ms(temps)
	

while 1: # Boucle infinie qui enclenche le radar s'il y'a eu appui du bouton, l'éteint si appui à nouveau
	if i == 1:
		d = sonar.distance_mm()/10
		print(d)
		
		# pas de son
		if d > 75:
			ch.pulse_width_percent(0)
			
		# son continu
		elif d < 5 and d > 0:
			buzz(0)
			
		# son évoluant en fonction de la distance
		else:
			t = int(d*5)
			buzz(t)
	# eteint le buzzer
	ch.pulse_width_percent(0)
	time.sleep_ms(5)

```

*Etape 1 :* Pour faire fonctionner le programme, nous devons dans un premier temps importer 2 bibliothèques. Pour se faire, il faut les importer au tout début de notre code de cette façon : 

```python
from us100 import US100
import time
```

*Etape 2 :*  On crée un objet correspondant au capteur qu'on nomme sonar :

```python
sonar=US100()
```

*Etape 3 :*  On affiche sur le terminal série la distance en cm que mesure le capteur toutes les secondes avec une boucle infinie :

```python
while True:
    print('Distance : %.1f cm ' % (sonar.distance_mm()/10))
    time.sleep(1)
```

**Résultat :**

En approchant la main ou un objet du capteur, on peut voir la distance qui évolue.

<div align="center">
<img alt="Affichage des données du capteur ultrason" src="images/US100_Putty.jpg" width="400px">
</div>

Les valeurs en -0,1cm correspondent à une erreur de mesure. Soit le capteur mesure une trop grande distance, soit l'objet est orienté de telle façon que le signal du capteur rebondit dessus mais est dirigé dans une autre direction et ne revient donc pas au capteur qui ne peut pas mesurer la distance.

**Pour aller plus loin :**

Nous avons développé un code que vous pouvez trouver en cliquant sur ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Ultrason/main_radar.py) qui simule le fonctionnement d'un capteur de recul automobile. Il faut ajouter un buzzer sur le port A3 du shield Grove comme montré ci-dessous. Le buzzer produit un son court qui se répète à une vitesse variant en fonction de la distance que détecte le capteur. Le buzzer commence à faire des sons pour une distance inférieure à 75cm et fait un son continue lorsque la distance est inférieure à 5cm.
Le détecteur de recul est activé/désactivé en appuyant sur le bouton SW1 de la carte qui fonctionne en interruption.
La led rouge LED1 s'allume lorsque le détecteur est activé.

<div align="center">
<img alt="Schéma de câblage du détecteur de recul" src="images/Radar_recul_cablage.jpg" width="400px">
</div>

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
