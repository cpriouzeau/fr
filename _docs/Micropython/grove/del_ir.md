---
title: LED infrarouge
description: Mise en oeuvre d'une LED (éventuellement infrarouge) Grove avec MicroPython
---

# LED infrarouge

Ce tutoriel explique comment faire clignoter une LED à une fréquence donnée, sur une broche numérique. Il s'applique à tout type de LED mais nous prenons ici l'exemple d'une LED infrarouge.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Une LED (infrarouge)

**La LED infrarouge de Grove :**

<div align="center">
<img alt="Grove - IR emitter" src="images/gove_IR_LED.jpg" width="385px">
</div>

**Le code MicroPython :**

Le code proposé est très simple et consiste à faire clignoter une LED en boucle après une initialisation de la broche D4 sur laquelle elle est connectée.

*Remarque* : Il est possible de vérifier que la LED clignote effectivement avec la caméra de votre téléphone qui est capable de percevoir le rayonnement IR !

```python
# Objet du code : Version MicroPyhton du programme "Blink".
# Fait clignoter une LED (éventuellement infrarouge) à une fréquence programmable.
# Nécessite une LED externe à la carte (module Grove par exemple).

import pyb
from pyb import Pin # Classe pour gérer les broches GPIO
import time # Classe pour temporiser

# La LED est assignéeà la broche D4
led = Pin('D4', Pin.OUT_PP)

while True :
	time.sleep_ms(500) # Pose 0.5 seconde
	led.off()
	print("LED éteinte")
	time.sleep(1) # Pause 1 seconde
	led.on() 
	print("LED allumée")
```
**Affichage sur le terminal série de l'USB User :**

<div align="center">
<img alt="Blink LED output" src="images/blink_output.png" width="400px">
</div>

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
