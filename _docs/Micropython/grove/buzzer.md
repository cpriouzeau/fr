---
title: Buzzer
description: Mise en oeuvre du buzzer Grove avec MicroPython
---

# Buzzer

Ce tutoriel explique comment mettre en ouevre un buzzer Grove à l'aide d'un signal modulé en largeur d'impulsion (PWM).

**Prérequis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un buzzer Grove

**Le Buzzer :**

Branchez le buzzer sur le connecteur D3 du GROVE base shield.
Celui-ci vibre et produit un son lorsqu'on lui transmet une tension.
Il est possible de modifier la fréquence du son _en générant un signal de type PWM_ sur la broche de commande du buzzer.

<div align="center">
<img alt="Grove buzzer" src="images/buzzer.png" width="200px">
</div>


**Qu'est-ce que la PWM ?**

La _modulation en largeur d'impulsion (PWM pour "Pulse Width Modulation")_ est une méthode qui permet de simuler un signal analogique en utilisant une source numérique.
On génère un signal carré, de fréquence donnée qui est à +3.3V (haut) pendant une proportion paramétrable de la période, et à 0V (bas) le reste du temps. C’est un moyen de moduler l’énergie envoyée à un actuateur. Usages : commander des moteurs, faire varier l’intensité d’une LED, faire varier la fréquence d’un buzzer... La proportion de la durée d'une période pendant laquelle la tension est dans l'état "haut" est appelée _rapport cyclique_. La figure ci-après montre tois signaux PWM avec des rapports cycliques de 25%, 50% et 75% (de bas en haut).

<div align="center">
<img alt="Grove buzzer" src="images/pwm.png" width="800px">
</div>

**Programmer des PWM sur la carte NUCLEO-WB55**

En pratique, la génération de signaux PWM est l’une des fonctions assurées par les _timers_, des composants intégrés dans le microcontôleur qui se comportent comme des compteurs programmables. Notre carte comporte plusieurs timers, et chaucun d'entre eux pilote plusieurs _canaux_ (_channels_ en anglais). Certains canaux de certains timers sont finalement connectés à des broches de sorties du Microcontôleur (GPIO). Ce sont ces broches là qui vont pouvoir être utilisées pour générer des signaux de commande PWM.

 Même si MicroPython permet de programmer facilement des broches en mode sortie PWM, vous aurez cependant besoin de connaitre :
 1. Quelles broches de la NUCLEO-WB55 sont des sorties PWM ;
 2. A quels timers et canaux csont connectées ces broches dans le STM32WB55.
 La figure ci-dessous répond à cette problématique :

<div align="center">
<img alt="PWM WB55" src="images/pwm_wb55.jpg" width="500px">
</div>

**Le code MicroPython :**

Le code consiste à faire lire "en boucle" au buzzer un tableau *frequency* encodant une gamme de notes musicales. Le canal et le timer de la broche PWM D3 utilisée sont identifiés à partir du plan de mappage de la figure ci-dessus. 

```python
# Objet du script : Jouer un jingle sur un buzzer (Grove ou autre).
# Cet exemple fait la démonstration de l'usage de la PWM pour la broche D3 sur laquelle est 
# branché le buzzer.

from pyb import Pin, Timer

# Liste des notes qui seront jouées par le buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 génère une PWM avec TIM1, CH3
BUZZER = Pin('D3') 

while True :
	# Itération entre 0 et 7
	for i in range (0,7) :
		# On ajuste la fréquence pendant l'itération
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		ch3.pulse_width_percent(5)	
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
