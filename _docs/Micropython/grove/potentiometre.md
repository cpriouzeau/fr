---
title: Potentiomètre
description: Mise en oeuvre du potentiomètre Grove avec MicroPython
---

# Potentiomètre

Ce tutoriel explique comment mettre en oeuvre un potentiomètre analogique Grove avec MicroPython.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un potentiomètre Grove

**Le potentiomètre Grove :**

<div align="center">
<img alt="Le potentiomètre Grove" src="images/potentiometre.png" width="300px">
</div>

Le potentiomètre, *rotary angle sensor* en anglais, permet de moduler manuellement une tension d’entrée comprise entre 0V et +3.3V (pour notre carte). Après conversion de cette tension (par l’ADC de la broche analogique sur laquelle est connecté le potentiomètre) le microcontrôleur reçoit une valeur entière comprise entre 0 et 4095.
Cette valeur peut ensuite servir à piloter d’autres actuateurs, par exemple à faire varier l’intensité sonore d’un buzzer.

Le potentiomètre est connecté sur la **broche / le connecteur A1**.

**Code MicroPython :**
```python
# Objet du script : Mise en oeuvre d'un potentiomètre.
# Exemple de configuration d'un ADC pour numériser une tension d'entrée variable grâce à un potentiomètre.
# Matériel requis : potentiomètre Grove (ou autre), idéalement conçu pour fonctionner entre 0 et 3.3V connecté sur A1. 

import pyb
from pyb import ADC, Pin
import time

print( "L'ADC avec MicroPython c'est facile" )

# Tension de référence de l'ADC, +3.3V
Vref = 3.3

# Valeur maxi de l'ADC 12 bits = 2^12 - 1  = 4095, pour un échantillonage entre 0 et 4095
Accuracy = 4095

# Initialisation de l'ADC sur la Pin A1
adc_A1 = ADC(Pin( 'A1' ))

while True:

	valeur_numerique = adc_A1.read()

	# Il faut maintenant convertir la valeur numérique par rapport à la tension de référence (3.3V) et
	# le nombre de bits du convertisseur (12 bits - 4096 valeurs)

	valeur_analogique = valeur_numerique * (Vref / Accuracy)

	print( "La valeur de la tension est :" , valeur_analogique, "V" )

	# Temporisation pendant 100ms
	time.sleep_ms(100)
````
