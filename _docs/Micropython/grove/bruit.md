---
title: Sonomètre
description: Mise en oeuvre du capteur de son Grove avec MicroPython
---

Ce tutoriel explique comment mettre en oeuvre un capteur de son Grove analogique avec MicroPython.

# Sonomètre

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un capteur de son Grove

**Le capteur de son Grove :**

<div align="center">
<img alt="Grove sound sensor" src="images/capteur_sonore.png" width="200px">
</div>

Ce capteur peut être utilisé comme détecteur de bruit ; sa sortie est proportionnelle au niveau sonore environnant.
Il doit être connecté sur une prise analogique du GROVE Base Shield, nous choisissons le **connecteur A1**. Cet exemple est une nouvelle illustration, très simple, de la conversion d'un signal en tension variable dans le temps (la réponse d'un microphone) en un signal numérique, via un ADC.


**Le code MicroPython :**
```python
# Objet du script : Mise en oeuvre du capteur de son Grove

import pyb
import time
from pyb import Pin

adc=ADC(PIN('A1'))

while True :
	time_sleep_ms(500) # Temporisation de 500 millisecondes
	print(adc.read())
```

Une valeur entière, entre 0 et 1024, proportionnelle à l'intensité du son ambiant, sera affichée dans l'interpréteur Python toutes les 0.5s.

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
