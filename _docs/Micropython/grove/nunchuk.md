---
title: Adaptateur Nintendo Nunchuk
description: Mise en oeuvre de l'adaptateur Gove pour manette Nintendo Nunchuk 
---

# Adaptateur Nintendo Nunchuk

Ce tutoriel explique comment mettre en oeuvre un adaptateur Gove pour manette Nintendo Nunchuk.

<h2>Description</h2>
La Nunchuk est une manette supplémentaire venant se connecter à la WiiMote (une manette destinée à être utilisée pour la console Nintendo Wii).
Elle comporte :

- Un joystick 2 axes (x, y)
- Un accéléromètre 3 axes (x, y, z)
- Des boutons poussoirs (C et Z)

Elle communique en liaison [I2C](https://stm32python.gitlab.io/fr/docs/Micropython/glossaire) avec la WiiMote ; un protocole de communication ne nécessitant que 4 fils (5V, GND, SDA, SCL).


<h2>Montage</h2>

La manette comporte un connecteur UEXT vers GROVE comme celui ci : 
<div align="center">
<img alt="Grove - Nintendo Nunchuk Adapter" src="images/grove-nunchuk.jpg" width="300px">
</div>

Il faut ensuite brnacher la manette à un connecteur I2C du Grove Base Shield.


<h2>Programme</h2>

Pour simplifier le code nous utilisons une bibliothèque externe disponible en suivant ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/wiichuck.py). Le fichier *main.py* est quant à lui disponible via ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/main.py).

**Etape 1 :** Pour faire fonctionner le programme nous devons dans un premier temps importer 3 bibliothèques au tout début de notre code de cette facon : 
```python
from machine import I2C, Pin
from time import sleep_ms
from wiichuck import WiiChuck
```

**Etape 2 :** Ensuite il faut les initialiser en rajoutant ces lignes de codes. 
La première ligne précise quel canal I2C nous utilisons (dans notre cas le canal 1). La deuxième ligne de code définit une variable *wii* qui récupèrera les informations retournées par la bibliothèque.

```python
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

wii = WiiChuck(i2c)
```

**Etape 3 :** Enfin nous affichons les informations retournées par la manette sous forme de tableau. Celui-ci sera affiché dans notre terminal et se fera dans une boucle infinie (CTRL+C pour l'interrompre).
```python
while True:
	direction = ''
	if wii.joy_up:
		direction = 'Haut'
	elif wii.joy_down:
		direction = 'Bas'
	elif wii.joy_right:
		direction = 'Droite'
	elif wii.joy_left:
		direction = 'Gauche'
	else:
		direction = '-----'
	if(wii.c):
		Cbouton = 'C'
	else:
		Cbouton = '-'
	if(wii.z):
		Zbouton = 'Z'
	else:
		Zbouton = '-'

	print("Joystick: (%3d, %3d) %6s \t| Accelerometre XYZ: (%3d, %3d, %3d) \t| Boutons: %s %s" %(wii.joy_x, wii.joy_y, direction, wii.accel_x, wii.accel_y, wii.accel_z, Cbouton, Zbouton))

	wii.update()
	sleep_ms(100)
```


<h2>Résultat</h2>

Et voilà ! Nous pouvons à présent observer le résultat avec les données extraites du Nunchuk sous cette forme :


<div align="center">
<img alt="Affichage des données du Nunchuk" src="images/nunchuk-python.png" width="800px">
</div><br>


Faites un mouvement avec le Nunchuk et vous devriez voir les valeurs de l'accéléromètre varier selon l'axe sur lequel vous l'avez déplacé. Vous pouvez également faire bouger le joystick et voir où celui-ci se situe. De plus vous avez un retour d'informations sur l'état des boutons poussoirs.


<h1>Pour aller plus loin</h1>

Dans cet exercice nous avons vu l'affichage des données du Nunchuk. Pour aller plus loin nous pouvons, par exemple, récupérer ces données et les traiter pour activer des servos moteurs en fonction de la direction du joystick.

Le but ici est de récupérer le déplacement du joystick sur l'axe horizontal (axe x) et faire tourner un servo-moteur de façon très précise.


<h2>Montage</h2>

On reprend le montage précédent avec la manette auquel ajoute un servo-moteur. Si vous n'avez pas encore abordée la partie sur le servo-moteur je vous invite à la consulter afin de mieux comprendre les valeurs utilisées dans cet exercice.

La correspondance entre le servo-moteur et la carte ST Nucleo est la suivante :

| Servo-moteur      | ST Nucleo         |
| :-------------:   | :---------------: |
|       Signal      |         D6        |
|       GND         |         GND       |
|       3.3V        |         3.3V      |


<h2>Programme</h2>

Pour simplifier le code nous (ré)utilisons une bibliothèque externe disponible en suivant ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/wiichuck.py). Le fichier *main.py* est quant à lui disponible via ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/main1.py).

**Etape 1 :** Pour faire fonctionner le programme nous devons dans un premier temps importer 4 bibliothèques au tout début de notre code de cette facon : 
```python
import pyb
from machine import I2C, Pin
from time import sleep_ms
from wiichuck import WiiChuck
```

**Etape 2 :** On initialise ensuite la connection avec le servo-moteur qui est connecté sur le port D6 et sur lequel on applique une fréquence de 50Hz (soit une période de 20ms) afin de pouvoir utiliser le moteur.
On initialise également la liaison I2C avec la manette en précisant quel canal I2C nous utilisons (dans notre cas le canal 1). Enfin on définit une variable *wii* qui récupèrera les informations retournées par la bibliothèque.
```python
#Initilisation servomoteur
servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)

#Initialisation Nunchuk
i2c = I2C(1)
sleep_ms(1000)
wii = WiiChuck(i2c)
```

**Etape 3 :** Nous utilisons une boucle infinie qui nous permettra de contrôler le servo-moteur en continu.
L'axe X du joystick peut avoir des valeurs comprises entre -100 (orienté à gauche) et 100 (orienté à droite) tandis que le *pulse_width_percent* du servo-moteur n'accepte que des valeurs comprises entre 2,5 et 12,5. Il faut donc trouver un rapport entre les deux. Pour un angle total plus petit que 180° on augmente la valeur du Joystick de +200 afin que toutes les valeurs deviennent positives et on divise le tout par 27.
De cette facon si on reprend les calculs avec la formule suivante :

**pulse_width_percent = (wii.joy_x + offset) / x**

Avec nos valeurs on obtient :
- Minimum : (-100 + 200) / 27 = **3,7%**	--> soit proche de 2,5% qui correspond à un angle de -90°
- Medium : (0 + 200) / 27 = **7,4%**		--> soit proche de 7,5% qui correspond à un angle de 0°
- Maximum : (100 + 200) / 27 = **11,1%**	--> soit proche de 12,5% qui correspond à un angle de 90°
Enfin vous l'avez compris, modifier ce calcul permettra de modifier les angles d'inclinaison du servo-moteur.

Dans notre cas nous avons fait exprès d'avoir des extrema moins importants que les vrais extrema car, avec la précision du calcul, le servo-moteur peut se retrouver en butée d'un côté mais pas de l'autre.
Cependant voici la formule si vous voulez essayer un angle (presque parfait) de 180° :

**(wii.joy_x + 150) / 20**

Et si on reprend les calculs on obtient : 
- Minimum : (-100 + 150) / 20 = **2,5%**
- Medium : (0 + 150) / 20 = **7,5%**
- Maximum : (100 + 150) / 20 = **12,5%**


Pour revenir à notre code :
```python
while True:
	joystick_x = (wii.joy_x + 200)/27
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=joystick_x)
	wii.update()
```


<h2>Résultat</h2>

Vous êtes maintenant en mesure de contrôler un servo-moteur à l'aide du joystick d'une manette Wii de facon assez simple. Vous pouvez également refaire la même chose mais en utilisant cette fois l'axe *y* du joystick pour controler un deuxième servo-moteur.
A partir de cette base vous pouvez même concevoir un mini-véhicule capable de se déplacer à droite ou gauche par le joystick et en utilisant le servo-moteur relié à un axe de direction des roues.


> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
