---
title: Variateur de lumière
description: Faire varier l'intensité lumineuse d'une LED avec MicroPython
---

# Variateur de lumière

Ce tutoriel explique comment faire varier l'intensité lumineuse d'une LED avec MicroPython en utilisant un signal de commande modulé en largeur d'impulsion (PWM). Vous trouverez une explication de ce qu'est la PWM [ici](buzzer).
Il s'agit de faire varier continuement l'intensité d'une LED. Pour cela, il est nécessaire de connecter la LED à *l'une des broches de la carte NUCLEO-WB55 qui supporte la fonction PWM*. Pour rappel, les broches possibles et les paramètres correspondants sont :

| | | |
|-|-|-|
**Broche**|**Timer**|**Canal** 
D0|2|4
D1|2|3
D3|1|3
D5|2|1
D6|1|1
D9|1|2
D11|17|1
D12|16|1
D13|2|1
D14|17|1
D15|16|1
A3|2|1
A2|2|2

**Prérequis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un module LED Grove.

**Premier code MicroPython : gestion du bouton en scrutation**

Dans ce premier exemple, l'intensité de la LED cesse de varier lorsque le bouton sw1 est maintenu appuyé. La gestion du bouton est faite par *scrutation* ("polling" en anglais).

```python
from pyb import Pin, Timer
import time

# Initialisation du bouton SW1
sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

button_pressed = False

# initialisation de la PWM 
p = Pin('D6') 
ti = 1 # Timer 
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i=0

while True:
	
	if not sw1.value(): # Si le bouton est appuyé
		
		while i < 101: # augmente l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i+1
			time.sleep_ms(10) # pause de 10 ms

		while i > 0: # réduit l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i-1
			time.sleep_ms(10) # pause de 10 ms
	
	else:
		ch.pulse_width_percent(0) # Si le bouton n'est pas appuyé
```

**Deuxième code MicroPython : gestion du bouton avec une interruption externe**

Dans ce deuxième exemple l'intensité de la LED cesse de varier après un premier appui sur le bouton sw1.
Elle recommence à varier après un second appui sur sw1. Le bouton est géré avec *une interruption externe*.

```python
from pyb import Pin, Timer, ExtInt
import time

# Initialisation du bouton SW1
sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# gestion interruption du bouton
button_pressed = False


# Routine de gesion de l'interruption du bouton
def ISR_bouton(line):
#	print("line =", line)
	global button_pressed # mot clef "global" très important ici
	button_pressed = not button_pressed

ext = ExtInt(Pin('SW1'), ExtInt.IRQ_RISING, Pin.PULL_UP, ISR_bouton)

# initialisation de la PWM 
p = Pin('D6') 
ti = 1 # Timer 
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i=0

while True:
	
	if button_pressed :
	
		while i < 101: # augmente l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i+1
			time.sleep_ms(10) # pause de 10 ms

		while i > 0: # réduit l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i-1
			time.sleep_ms(10) # pause de 10 ms
```
