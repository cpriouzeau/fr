---
title: Joystick
description: Mise en oeuvre du "thumb-joystick" Grove avec MicroPython
---

# Joystick

Ce tutoriel explique comment mettre en oeuvre un "thumb-joystick" Grove avec MicroPython.

**Matériel requis :**

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un "thumb-joystick" Grove

**Le thumb-joystick de Grove :**

<div align="center">
<img alt="Grove thumb-joystick" src="images/joystick.png" width="200px">
</div>


Ce joystick est similaire à ceux que l'on peut retrouver sur une manette de PS2. Chacun des axes est relié à un potentiomètre  qui va fournir une tension proportionnelle au mouvement. Il possède de plus un bouton poussoir.
Le code permet de vérifier tous les états que le joystick peut prendre. On pourra vérifier nos inputs dans l'interpréteur Python.
Le joystick doit être branché sur un connecteur analogique A0, A1, A2 ou A3 (voir commentaires dans le code ci-dessous).


**Le code MicroPython :**
```python
# Objet du script : Mise en oeuvre du Joystick Grove.

import pyb
import time # Bibliothèque pour temporiser
from pyb import Pin

vertical = ADC(PIN('A0')) #en branchant le joystick sur A0,l'axe Y sera lu par A0 et l'axe X par A1
horizontal= ADC(PIN('A1')) #sur A1, Y sera lu par A1 et X par A2; sur A2, Y sera lu par A2 et X par A3..

while True :

	time_sleep_ms(500)
	
	x=vertical.read()
	y=horizontal.read()

	if (x<=780 && x>=750) :
		print("Haut")
	if (x<=280 && x>=240) :
		print("Bas")
	if (y<=780 && y>=750) :
		print("Gauche")
	if (y<=280 && y>=240) :
		print("Droite")
	if (x>=1000) : #en appuyant sur le joystick, la sortie de l'axe X se met à 1024, le maximum.
		print("Appuyé")
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
