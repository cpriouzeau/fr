---
title: Le kit de développement NUCLEO-WB55
description: Le kit de développement NUCLEO-WB55

---
# Le kit de développement NUCLEO-WB55

## Aperçu de la carte NUCLEO-WB55

Le microcontrôleur STM32WB55 rassemble sur la même puce en silicium deux microprocesseurs ARM Cortex ultra - basse consommation et offre une connectivité radio Bluetooth à basse consommation (BLE pour "Bluetooth Low Energy").

La carte NUCLEO-WB55 permet de programmer aisément le microcontrôleur STM32WB55 qui l'équipe en donnant accès à toutes ses fonctions. 

Les connecteurs d’extensions (Arduino, Morpho) permettent à l'utilisateur de connecter des composants électroniques externes au STM32WB55.

La carte  dispose aussi d’un connecteur pour pile CR2032, de 3 boutons (SW1,SW2,SW3), de 3 LED (rouge, verte et bleue) et de 2 connecteurs micro-USB.

_Voici le schéma bloc du kit NUCLEO-WB55:_

## Description de la carte NUCLEO-WB55

<div align="center">
<img alt="NUCLEO-WB55" src="images/board.png" width="1200px">
</div>

### Antenne PCB

C’est l’antenne Bluetooth du microcontrôleur STM32WB55. Elle est dite "PCB" (pour "Printed Circuit Board") car elle est intégrée directement dans la plaque d'époxy de la carte Nucleo STM32WB55. Sa forme complexe est conçue de sorte à optimiser la réception et l’émission des ondes radio pour la fréquence du Bluetooth soit 2,4 GHz. 

### Connecteurs Arduino et connecteurs Morpho 

Des kits d’extensions peuvent être ajoutés simplement grâce à ces connecteurs normalisés. On pourra, par exemple, brancher le kit Mems Microphone (X-NUCLEO-CCA02M2) sur la carte NUCLEO-WB55 :

<div align="center">
<img alt="Carte Nucleo WB55 et shield X-NUCLEO-CCA02M2" src="images/boards.png" width="400px">
</div>

Les connecteurs Morpho permettent d'accéder à la plupart des fonctions du microcontrôleur STM32WB55. Cependant nous utiliserons seulement les connecteurs Arduino. Les cartographies ci-dessous situent les fonctions secondaires qui nous seront utiles sur les connecteurs Arduino. Le câblage de notre carte NUCLEO-WB55 est presque identique à celui des cartes Arduino UNO à l'exception notable des broches PWM.

<div align="center">
<img alt="Arduino mapping" src="images/arduino_connectors_mapping.png" width="900px">
</div>


### STM32WB55

Le Microcontrôleur qui anime la carte. C'est un système sur puce (SoC) avec deux coeurs ARM Cortex, un dédié aux programmes de l'utilisateur et un dédié à la gestion de la communication selon le protocole BLE.

### Bouton reset

Lorsqu'on appuie dessus, le microcontrôleur interrompt son travail en cours et lance sa séquence d'initialisation. 

### Level Shifter

Circuit spécialisé dans la conversion entre deux niveaux de tensions.

### USB User

C’est le port USB qui nous servira à:

*   Communiquer avec l'interpréteur MicroPython 
*   Programmer le système
*   Alimenter le kit de développement

### Compartiment CR2032

Une fois le système programmé, il sera possible d’alimenter le kit par une pile CR2032 afin de rendre l'ensemble portable.

#### ST-LINK

C’est l’outil qui permet de programmer le microcontrôleur, pour cela utiliser le "glisser-déposer" pour déposer le firmware sur le périphérique USB : **WB55-NODE**. 

### LED utilisateur (User LED)

Ces trois LED, une rouge, une bleue et une verte, sont accessibles au développeur et pourront être commutées grâce à MicroPython ; nous les utiliserons tout au long des exercices présentés par la suite.

## Complément : mappage et désignation des broches pour les connecteurs Arduino et Morpho

<br>
<div align="center">
<img alt="Arduino mapping" src="images/pins.png" width="500px">
</div>
<br>

<div align="center">
<img alt="Arduino mapping" src="images/tableau1.png" width="500px">
</div>
<div align="center">
<img alt="Arduino mapping" src="images/tableau2.png" width="500px">
</div>
