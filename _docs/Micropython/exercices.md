---
title: Exercices pour démarrer avec MicroPython et la NUCLEO-WB55
description: Exercices pour démarrer avec MicroPython et la NUCLEO-WB55

---
# Exercices pour démarrer avec MicroPython et la NUCLEO-WB55

**L'ensemble des codes sources sont disponibles dans la [zone de téléchargement](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement).**<br>

## Lire l’état d’un bouton (GPIO)

Trois boutons SW1, SW2 et SW3 sont disponibles pour le développeur : 

<div align="center">
<img alt="Boutons" src="images/boutons.jpg" width="500px">
</div>

Nous allons voir dans cette sous-partie comment initialiser une Pin (broche) en mode "Entrée" et afficher un message lors de l’appui sur l'un des 3 boutons en utilisant **pyb.Pin**. 

Nous utiliserons la méthode dite de "pulling" afin de demander au système MicroPython l’état de la Pin (1 ou 0). 

Pour des raisons de conception électronique, l’état de la Pin au repos, bouton relâché, est "1" (potentiel fixé à +3.3V) alors que l’état lors d’un appui bouton est "0" (potentiel fixé à 0V).

*   Ouvrez l’éditeur de script et modifiez le fichier ***main.py***: 

```python
# Objet du script :
# Exemple de configuration des GPIO pour une gestion des boutons de la carte NUCLEO-WB55

import pyb # Bibliothèque de MicroPython permettant les accès aux périphériques (GPIO, LED, etc.)
import time # Bibliothèque de MicroPython permettant de faire des pauses systèmes

print( "Les GPIO avec MicroPython c'est facile" )

# Initialisation des broches d'entrées pour les boutons (SW1, SW2, SW3)
# Le potentiel des broches sera à +3.3V lorsque les boutons seront relâchés (Pull up)
# Le potentiel des broches sera à 0V lorsque les boutons seront enfoncés

sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1) 

sw2 = pyb.Pin( 'SW2' , pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

sw3 = pyb.Pin( 'SW3' , pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Initialisation des variables
ancienne_valeur_sw1 = 0
ancienne_valeur_sw2 = 0
ancienne_valeur_sw3 = 0

while True: # Boucle sans clause de sortie ("infinie")

	# Temporisation pendant 300ms
	time.sleep_ms(300)
	
	#Récupération de l'état des Boutons 1,2,3
	valeur_sw1 = sw1.value()
	valeur_sw2 = sw2.value()
	valeur_sw3 = sw3.value()
	
	#L'état courant est il différent de l'état précédent ?
	if valeur_sw1 != ancienne_valeur_sw1:
		if valeur_sw1 == 0:
			print( "Le bouton 1 (SW1) est appuyé" )
		else :
			print( "Le bouton 1 (SW1) est relâché" )
		ancienne_valeur_sw1 = valeur_sw1
	if valeur_sw2 != ancienne_valeur_sw2:
		if valeur_sw2 == 0:
			print( "Le bouton 2 (SW2) est appuyé" )
		else :
			print( "Le bouton 2 (SW2) est relâché" )
		ancienne_valeur_sw2 = valeur_sw2
	if valeur_sw3 != ancienne_valeur_sw3:
		if valeur_sw3 == 0:
			print( "Le bouton 3 (SW3) est appuyé" )
		else :
			print( "Le bouton 3 (SW3) est relâché" )
		ancienne_valeur_sw3 = valeur_sw3
```

*   Enregistrez le script *main.py* (CTRL + S), puis redémarrez la carte (CTRL+D). 

Lorsque vous appuyez sur l'un des 3 boutons (SW1, SW2, SW3), des messages dans la console vous indiquent leur état :

<div align="center">
<img alt="chenillard" src="images/switchs.png" width="700px">
</div>

## Changer l’état d’une sortie pour piloter une LED (GPIO)

L’objectif est maintenant d’allumer ou éteindre les LED du kit de développement :

<div align="center">
<img alt="LEDS" src="images/led.jpg" width="500px">
</div>

Voici l’organisation des LED par couleur et numéro: 

1. LED 1 : Bleu
2. LED 2 : Vert
3. LED 3 : Rouge

Sous MicroPython, le module **pyb.LED** permet de gérer les LED très simplement. 

Dans cette partie, nous voulons réaliser un “chenillard”, cet exercice consiste à allumer puis éteindre les LED les unes après les autres, de façon cyclique. 

*   Le chenillard est réalisé avec le script MicroPython suivant: 

```python
# Objet du script : Créer un "chenillard"
# Exemple de configuration des GPIO pour une gestion des LED intégrées de la carte NUCLEO-WB55

import pyb # Bibliothèque de MicroPython permettant les accès aux périphériques (GPIO, LED, etc.)
import time # Bibliothèque de MicroPython permettant de faire des pauses systèmes

print( "Les LEDs avec MicroPython c'est facile" )

# Initialisation des LEDs (LED_1, LED_2, LED_3)
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

# Initialisation du compteur de LED
compteur_de_led = 0

while True: # Création d'une boucle "infinie" (pas de clause de sortie)
	if compteur_de_led == 0:
		led_bleu.on()
		led_rouge.off()
		led_vert.off()
	elif compteur_de_led == 1:
		led_bleu.off()
		led_vert.on()
		led_rouge.off()
	else :
		led_bleu.off()
		led_vert.off()
		led_rouge.on()
	# On veut allumer la prochaine LED à la prochaine itération de la boucle
	compteur_de_led = compteur_de_led + 1
	if compteur_de_led > 2:
		compteur_de_led = 0
	time.sleep_ms(500) # Temporisation de 500ms
```

Vous pouvez observer les 3 leds qui s'allument puis s'éteignent selon la séquence LED3 -> LED2 -> LED1 -> LED3 -> LED2...

## Utilisation des interruptions

Nous allons reprendre le code précédent avec le chenillard de LED en ajoutant la possibilité d'appuyer sur le bouton SW1 pour mettre le chenillard en pause ou sur le bouton SW2 pour faire changer le sens du chenillard.

L'intérêt d'utiliser la lecture des boutons sous forme d'interruption est que l'action sur les boutons sera détectée même si une autre action est effectuée en même temps (ou pourra par exemple changer le sens du chenillard quand celui-ci est en pause).

*	L'exemple est réalisé avec le code suivant :

```python
# Objet du script : Créer un "chenillard" avec interruptions
# Exemple de configuration des GPIO pour une gestion des LED intégrées de la carte NUCLEO-WB55

import pyb # Bibliothèque de MicroPython permettant les accès aux périphériques (GPIO, LED, etc.)
import time # Bibliothèque de MicroPython permettant de faire des pauses systèmes

print( "Les interruptions avec MicroPython c'est facile" )

# Initialisation des LEDs (LED_1, LED_2, LED_3)
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

# Initialisation des variables globales
compteur_de_led = 0

#flags des interruptions
pause = 0
inv = 0

# Initialisation des boutons (SW1 et SW2)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2')
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#Fonction d'interruption de SW1 (met en pause le chenillard)
def Pause(line):
	global pause
	if(pause == 0):
		pause = 1
		print("Pause")
	else:
		pause = 0

#Fonction d'interruption de SW2 (Inverse le sens du chenillard)
def Inversion(line):
	global inv
	if(inv == 0):
		inv = 1
	else:
		inv = 0
	
# Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, Pause)
irq_2 = pyb.ExtInt(sw2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, Inversion)

while True: # Création d'une boucle "infinie" avec des actions uniquement si le système n'est pas en pause
	if (pause == 0):
		if compteur_de_led == 0:
			led_bleu.on()
			led_rouge.off()
			led_vert.off()
		elif compteur_de_led == 1:
			led_bleu.off()
			led_vert.on()
			led_rouge.off()
		else :
			led_bleu.off()
			led_vert.off()
			led_rouge.on()
		
		# On veut allumer la prochaine LED à la prochaine itération de la boucle avec gestion du sens
		if (inv==0):
			compteur_de_led = compteur_de_led + 1
			if compteur_de_led > 2:
				compteur_de_led = 0
		else:
			compteur_de_led = compteur_de_led - 1
			if compteur_de_led < 0:
				compteur_de_led = 2
		time.sleep_ms(500) # Temporisation de 500ms
```

Pour utiliser les interruptions, il est nécessaire d'initialiser les boutons comme nous avons vu précédemment. Il faut ensuite définir les vecteurs d'interruption propres à chaque bouton avec la fonction **pyb.ExtInt()** et les fonctions qui seront appelées à chaque interruption.

Comme une fonction d'interruption doit être courte pour ne pas ralentir le programme, nous utilisons des variables globales appelées "flag" qui sont modifiées à chaque entrée dans une fonction d'interruption et qui ont des incidences dans la fonction *main*.

## Lecture d’une valeur analogique (ADC)

Nous aimerions maintenant convertir la valeur analogique (0-3.3V) d’un signal en valeur numérique (0-4095). 

Vous pourrez brancher sur l'une des broches Arduino A0 à A5 un périphérique renvoyant une tension comprise entre 0 et +3.3V (éventuellement variable au cours du temps). Ce connecteur est directement relié à l’un des ADC du microcontrôleur qui va procéder à la conversion du signal analogique en un signal numérique :

<div align="center">
<img alt="Sortie ADC" src="images/adc.jpg" width="500px">
</div>

Pour cette démonstration nous utiliserons un potentiomètre (10 KOhm) du commerce que nous brancherons sur A0, comme ceci : 

<div align="center">
<img alt="potentiometre" src="images/potentiometre.png" width="300px">
</div>

N’importe quelle référence de potentiomètre 10 KOhm fonctionne pour la démonstration, par exemple [PTV09A-4020F-B103](https://www.mouser.fr/ProductDetail/Bourns/PTV09A-4020F-B103?qs=sGAEpiMZZMtC25l1F4XBU1xwXnrUt%2FuoeIXuGADl09o%3D) :

<div align="center">
<img alt="potentiometre2" src="images/potentiometre2.png" width="70px">
</div>

Avec l’aide du script suivant, utilisez la fonction *pyb.ADC* pour interagir avec l’ADC du STM32WB55 :

```python
# Objet du script :
# Exemple de configuration d'un ADC pour numériser une tension d'entrée variable grâce à un potentiomètre.
# Pour augmenter la précision, on calcule la moyenne de la tension d'entrée sur 500 mesures (1 mesure / milliseconde)
# Matériel requis : potentiomètre (Grove ou autre), idéalement conçu pour fonctionner entre 0 et 3.3V connecté sur A0. 

import pyb
import time

print( "L'ADC avec MicroPython c'est facile" )

# Tension de référence de l'ADC, +3.3V
Vref = 3.3

# Valeur maxi de l'ADC 12 bits = 2^12 - 1  = 4095, pour un échantillonage entre 0 et 4095
Accuracy = 4095

# Ratio pré-calculé, pour ne pas le recalculer inutilement à chaque conversion
ratio = Vref / Accuracy

# Initialisation de l'ADC sur la broche A0
adc_A0 = pyb.ADC(pyb.Pin( 'A0' ))

# Initialisations pour calcul de la moyenne
Nb_Mesures = 500
Inv_Nb_Mesures = 1 / Nb_Mesures

while True: # Boucle "infinie" (sans clause sortie)
	
	somme_tension = 0
	moyenne_tension = 0
	
	for i in range(Nb_Mesures): # On fait Nb_Mesures conversions de la tension d'entrée
		
		# Lit la conversion de l'ADC (un nombre entre 0 et 4095 proportionnel à la tension d'entrée)
		valeur_numerique = adc_A0.read() 
		
		# Il faut maintenant convertir la valeur numérique par rapport à la tension de référence (3.3V) et
		# le nombre de bits du convertisseur (12 bits - 4096 valeurs)
		valeur_analogique = valeur_numerique * ratio
		
		somme_tension = somme_tension + valeur_analogique
		# Temporisation pendant 1 ms
		time.sleep_ms(1)
	
	moyenne_tension = somme_tension * Inv_Nb_Mesures # On divise par Nb_Mesures pour calculer la moyenne de la tension d'entrée
	
	print( "La valeur de la tension (moyenne) est :" , moyenne_tension, "V" )
```

Vous pouvez lancer le terminal Putty et observer la valeur en volts qui évolue, toutes les 500ms, lorsque vous tournez le potentiomètre :

<div align="center">
<img alt="Sortie Potentiometre" src="images/output_potentiometre.png" width="700px">
</div>

## Piloter un afficheur OLED (I2C)

Il est très facile d’utiliser un écran OLED avec MicroPython pour afficher des messages. Nous verrons dans cet exemple comment brancher l’écran sur le bus I2C, puis comment le piloter pour y envoyer des messages avec MicroPython. 

Pour l'exemple, nous utiliserons l’écran monochrome OLED, 192 x 32 pixels de Adafruit, cependant tous les écrans intégrant le driver SSD1306 sont compatibles.

Voici comment brancher l’écran OLED : 
* D15=SCL
* D14=SDA

<div align="center">
<img alt="oled" src="images/oled.png" width="400px">
</div>

Nous aurons besoin du fichier *ssd1306.py* disponible en téléchargement. Il se trouve dans le dossier *Piloter un afficheur OLED (I2C)* de l'archive [ZIP](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement). 

Lorsque vous aurez terminé le téléchargement, il faudra transférer le fichier dans le répertoire du périphérique *PYBLASH*. 

*   Editez maintenant le script ***main.py***:

```python
from machine import Pin, I2C
import ssd1306
import time

#Initialisation du périphérique I2C
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

#Paramétrage des caractéristiques de l'écran
largeur_ecran_oled = 128
longueur_ecran_oled = 32
oled = ssd1306.SSD1306_I2C(largeur_ecran_oled, longueur_ecran_oled, i2c)

#Envoi du texte à afficher sur l'écran OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('Trop facile !!!', 0, 20)
oled.show()
```
## Utilisation Bluetooth Low Energy (BLE)

Dans cette partie nous allons voir comment communiquer en Bluetooth Low Energy avec l’application STBLESensor et la carte de développement NUCLEO-WB55. 

### Installation de ST BLE Sensor sur votre smartphone

Installez sur votre smartphone l’application STBLESensor disponible sur [Google Play](https://play.google.com/store/apps/details?id=com.st.bluems) ou sur [IOS Store](https://apps.apple.com/it/app/st-ble-sensor/id993670214)

![image](images/stmblesensorapp.png) ![Android](images/stmblesensorapp-qr-android.png)   ![iOS](images/stmblesensorapp-qr-ios.png)

Consultez [la description complète des différents services proposés par l’application STBLESensor](https://github.com/STMicroelectronics/STBlueMS_Android)<br>

### Communication BLE en MicroPython

Pour communiquer en Bluetooth Low Energy avec MicroPython, il faudra inclure 2 nouveaux fichiers dans le répertoire du disque USB *PYBFLASH* :
1. ***ble_advertising.py*** (Fichier d’aide à la création du message d’advertising)
2. ***ble_sensor.py*** (Classe permettant la gestion de la connexion BLE)

Il faudra télécharger les scripts nécessaires à cet exemple. Ils se trouvent dans le dossier *Utilisation du Bluetooth Low Energy (BLE)/Température* de l'archive [ZIP](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement).

Grâce au fichier *ble_sensor.py*, nous allons créer un objet BLE ayant 1 service et 2 caractéristiques. _C’est ce fichier qu’il faudra modifier pour changer le profil BLE, si besoin_.

La modification de ble_sensor.py est un peu délicate et nécessite de connaitre le protocole [BlueST documenté ici](https://www.st.com/resource/en/user_manual/dm00550659-getting-started-with-the-bluest-protocol-and-sdk-stmicroelectronics.pdf). Ce sujet dépasse le cadre l'initiation qui nous intéresse, mais vous trouverez néanmoins quelques explications sur la façon de procéder pour "caster" simultanément plusieurs types de données (*Température + Humidité* et *Température + Humidité + Pression*) en BLE selon le protocole BlueST dans les exemples en téléchargement (brièvement : il s'agit de recalculer un "masque binaire" qui se trouve au début du fichier ble_sensor.py).

*   Editez maintenant le script ***main.py***:

```python
# Objet du script :
# Casting en BLE d'une valeur aléatoire simulant une température.
# L'application smartphone STBLESensor est requise pour afficher cette grandeur.

import struct
import bluetooth # Classe Micropython gérant le protocole BLE 
import ble_sensor # Classe pour implémentation du protocole BlueST
import time
import random # Classe pour générer des nombres aléatoires

ble = bluetooth.BLE() # Instance de la classe BLE
ble_device = ble_sensor.BLESensor(ble)  # Instance de la classe BlueST

while True: # Boucle sans clause de fin, infinie (tant qu'il y a du courant !)
	timestamp = time.time()
	# valeur aléatoire entre 0 et 100 °C
	temperature = (random.randint(0, 1000))  
	# envoi en BLE du timestamp et de la température en choisissant de notifier l'application
	ble_device.set_data_temperature(timestamp, temperature, notify=1) 
	time.sleep_ms(1000)
```

Une fois ce script lancé, la carte NUCLEO-WB55 se met à émettre des trames BLE, de type *advertising*. Ces messages permettent d’identifier l’objet Bluetooth et de signaler qu'il est disponible pour être connecté.

Le nom du périphérique émulé par le STM32WB55 est : ***WB55-MPY***. Nous allons vérifier avec l’application smartphone que la carte NUCLEO-WB55 est effectivement en mode émission Bluetooth.

### Connexion à STBLESensor
Lancez l’application ***STBLESensor*** sur votre smartphone :

![image](images/app1.png)

* Appuyez ensuite sur l’icône loupe pour afficher les périphériques BLE environnants :

![image](images/app2.png)

Dans cet exemple, le profil BLE que nous avons choisi nous permet de simuler un thermomètre et d’allumer ou d’éteindre une LED. La valeur du thermomètre est simulée, générée aléatoirement toutes les secondes. Des exemples plus réalistes, qui envoient en BLE les mesures des capteurs environnementaux de la carte X-NUCLEO-IKS01A3 par exemple, sont disponibles en téléchargement. Ils se trouvent dans le dossier *Utilisation Bluetooth Low Energy (BLE)* de l'archive [ZIP](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement). 

* Connectez-vous à la carte de développement en appuyant sur "WB55-MPY":

![image](images/app3.png)

La LED bleue de la carte WB55 doit s’allumer lorsqu’elle est connectée à l’application.
Nous pouvons observer, sur cet écran, l’évolution aléatoire de la température entre 0 et 100 °C.
Il est possible d’afficher la température en mode graphique.

* Pour cela appuyez sur le bouton menu ![image](images/app-menu.png):

![image](images/app4.png)

* Appuyez maintenant sur ![image](images/app-plot.png):

![image](images/app5.png)

* Pour afficher le graphique, appuyez sur ![image](images/app-play.png):

![image](images/app6.png)

Vous pouvez utiliser le bouton ![image](images/app-options.png) pour modifier les options du graphique, comme la taille de l’axe X ou l’activation du changement automatique de l’échelle en Y.

Nous allons maintenant étudier l'envoi d'une information depuis le smartphone vers la carte NUCLEO-WB55.
Pour cela nous utilisons l’application pour allumer ou éteindre sa LED rouge.

* Pour cela appuyez sur le bouton menu ![image](images/app-menu.png):

![image](images/app7.png)

* Choisissez maintenant l’option ![image](images/app-switch.png):

![image](images/app8.png)

Vous pouvez sur cet écran piloter la LED Rouge de la carte NUCLEO-WB55.

