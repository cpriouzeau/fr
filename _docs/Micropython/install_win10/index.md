---
title: Guide de démarrage rapide sous Windows 10
description: Guide de démarrage rapide sous Windows 10

---
# Guide de démarrage rapide sous Windows 10

> Ces étapes sont à réaliser par le professeur ayant un accès administrateur sous Windows 10, pour chaque carte NUCLEO-WB55 utilisée en TP. 

Dans ce guide de démarrage, nous allons:
- Programmer la carte avec le micrologiciel
- Utiliser un terminal de communication UART (TeraTerm ou PUTTY)
- Programmer sur la NUCLEO notre premier script Python

## Configuration électronique

**Vous aurez besoin d’un câble USB vers micro-USB.**

Cette configuration se résume à :

- Assurez-vous que le cavalier est positionné sur ***USB_STL***
- Connectez un câble micro USB sur le port ***ST_LINK*** en ***dessous des LED4 et LED5***

Voici comment doit être configuré le kit :<br>

<div align="center">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="400px">
</div>

Ces modifications permettent de configurer le STM32WB55 en mode programmation "ST Link".<br>
Nous pourrons ainsi mettre à jour le firmware (fichier binaire) en passant par le connecteur USB_STL.

## Programmation des cartes

Téléchargez et stockez le [Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) (fichier .bin)

Reliez la carte NUCLEO-WB55(USB_STLINK) avec le câble USB à votre ordinateur.

### Installation du micrologiciel

Ouvrir le lecteur ***NOD_WB55***<br>

![image](images/Nod_wb55.PNG)

Faire un glisser-déposer du fichier [Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) (fichier .bin).<br>
Le micrologiciel est maintenant intégré (il n'est plus visible dans le lecteur NOD_WB55)

Placez le cavalier sur ***USB_MCU*** ainsi que le câble micro USB sur le connecteur ***USB_USER*** sous le ***bouton RESET***<br>

<div align="center">
<img alt="Nucleo_WB55_Config_MCU" src="images/Nucleo_WB55_Config_MCU.png" width="400px">
</div>

***Redémarrez la carte NUCLEO***

Votre carte NUCLEO est maintenant capable d'interpréter un fichier ".py".

Regardons les fichiers générés par le système MicroPython avec l'explorateur Windows :

Ouvrir le périphérique PYBFLASH avec l'explorateur Windows :<br>

![Image](images/dfu_util_3.png)

Nous verrons plus tard comment éditer les scripts Python disponibles dans le système de fichier PYBFLASH.

**Remarques importantes :** 
Le script `boot.py` peut être modifié par les utilisateurs avancés, il permet d'initialiser MicroPython et notamment de choisir quel script sera exécuté après le démarrage de MicroPython, par défaut le script `main.py`.<br>

## Installation de l'environnement de programmation

Trois solutions s'offrent à vous:
- Utiliser la version portable ou installer (droits admin requis) [Notepad++](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) avec la coloration synthaxique Python et un terminal série ([PuTTY](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement), [TeraTerm](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)...)
- Installer (droits admin requis) [Thonny](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) et son terminal integré
- Utiliser la version portable de [PyScripter](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) et un terminal série ([PuTTY](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement), [TeraTerm](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargementn)...).


## Configuration du terminal de communication

Maintenant que MicroPython est présent sur le kit NUCLEO-WB55, nous voudrions tester la communication avec Python. Le test consiste à envoyer une commande Python et à vérifier que l'exécution se déroule correctement.

La communication s'effectue par USB par le biais d'un port série, nous avons donc besoin d'un logiciel capable d'envoyer les commandes Python sous forme de texte à la carte et de recevoir le résultat de l'exécution.

Ouvrir TeraTerm () après l'avoir installé ou bien PuTTY "portable" si vous n'avez pas les droits d'administration.

Sélectionnez l'option ***Serial*** et le ***port COM*** correspondant à votre carte (**COMx: USB Serial Device**) :<br>

![Image](images/TeraTerm.PNG)<br><br>

Si le COM correspondant à la carte n'est pas disponible, vérifiez que le kit est bien énuméré sur le bon port COM, voir **Vérifier l'attribution du port COM**.

Configurez les champs suivants dans le menu ***Setup*** / ***Serial Port***<br><br>

![Image](images/TeraTerm_Setup.png)

Une nouvelle fenêtre s'affiche :<br><br>

![Image](images/TeraTermDisplay_1.png)

Si nécessaire, appuyez sur *CTRL+D* pour faire un **Reset software**.

Vous pouvez maintenant exécuter des commandes Python (en mode REPL).<br>
```python
print("Hello World")
```

![Image](images/TeraTermDisplay_2.png)

Nous avons maintenant terminé la programmation du firmware MicroPython.<br>
Nous pouvons exécuter des fonctions Python au travers du port COM.

Notre kit de développement NUCLEO-WB55 est désormais prêt à être utilisé avec Python.


Gardez la fenêtre TeraTerm ouverte, elle vous sera utile pour visualiser l'exécution des scripts.


## Ecrire et exécuter un script MicroPython avec Notepad++

Nous allons voir dans cette partie comment éditer avec Notepad++ et exécuter un script Python sur notre carte NUCLEO-WB55. Lancez Notepad++ après l'avoir installé (vous pouvez aussi utiliser la version portable).
Cet outil vous permettra d'éditer vos scripts Python.<BR>

Ouvrez le fichier ***main.py*** présent sur la Nucleo (représenté par le lecteur **PYBFLASH**).

Notre premier script va consister à afficher 10 fois le message `MicroPython est génial` avec le numéro du message à travers le port série du connecteur USB user de la carte NUCLEO.

Ecrivez l'algorithme suivant dans l'éditeur de script :<br>

![Image](images/Notepad_test1.png)

Prenez soin de sauvegarder le fichier (*CTRL+S*) ***main.py*** sur la carte (lecteur PYBFLASH).<br>
Faites un software reset de la carte en appuyant sur *CTRL+D* dans votre terminal série (TeraTerm par exemple), le script est directement interprété !<br>

![Image](images/TeraTerm_test1.png)

Le script s'est exécuté avec succès ; nous voyons bien notre message s'afficher 10 fois dans le terminal.<br>
Vous venez d'écrire votre premier script Python embarqué dans une carte NUCLEO-WB55!<br> 

**Remarques importantes :**
- Voici la [liste des libraires utilisables ou en cours de développement sous MicroPython](http://docs.micropython.org/en/latest/library/index.html#micropython-specific-libraries).<br>
- Notepad++ n'est pas configuré par défaut pour utiliser des tabulations ; il les simule par plusieurs espaces consécutifs. Pour le codage en MicroPython, il est préférable de modifier ce comportement.
    - On commence par forcer l'affichage des espaces et tabulations, en allant dans le menu "View" (ou "Affichage" pour la version française), puis dans le sous-menu "Show Symbol" (VF : "Symboles spéciaux" où on coche les options "Show White Space and TAB" (VF : "Afficher les blancs et les tabulations") et "Show Indent guide" (VF : "Afficher le guide d'indentation").<br><br>
![Image](images/Menu_1.png)
    - Ensuite, on demande à Notepad++ d'effectivement remplacer les espaces par des tabulations dans les scripts Python, en allant dans le menu "Settings" (VF : "Paramètres"), puis dans la boite "Preferences" (VF : "Préférences"). Dans la colonne de gauche il faut sélectionner "Language" (VF : "Langage"). Dans l'encadré à droite "Tab Setting" (VF : "Tabulations") on sélectionne "python" et enfin dans l'encadré "Use default value" (VF : "Valeur par défaut") il faut désactiver la case "Replace by space" (VF : "Insérer des espaces").<br><br>
![Image](images/Menu_2.png)

## Annexe 1 : Précautions à prendre pour ne pas corrompre le système de fichiers MicroPython

Vous pourrez rencontrer occasionellement des difficultés lorsque vous modifiez le fichier main.py ou d'autres fichiers situés sur le lecteur PYBFLASH. Le système de fichiers de celui-ci, de type FAT, peut être accidentellement corrompu si vous ne respectez pas quelques bonnes pratiques. Afin d'éviter ces problèmes de corruption de la FAT du lecteur PYBFLASH, nous vous **recommandons de prendre les précautions suivantes** :<BR>

- Quand vous éditez un fichier sur PYBFLASH, **NE PAS OUBLIER DE SAUVEGARDER LE FICHIER AVANT D’ENLEVER LE CABLE USB DU PC.**
- Quand vous voulez déconnecter le câble USB du PC, **NE PAS OUBLIER DE DEMONTER/EJECTER PROPREMENT PYBFLASH.**
- Quand vous voulez tester une nouvelle version de votre code, **SVP UTILISEZ CTRL+C ET CTRL+D DANS LA CONSOLE SERIE.**<br>
    - CTRL+C : pour arrêter l’exécution courante.
    - CTRL+D : Demande à MicroPython de faire une synchronization du système de fichiers FAT et un RESET logiciel.
- Quand vous voulez tester une nouvelle version de votre code et que MicroPython indique qu’il y a une erreur (non visible) dans le code, **VOUS DEVEZ DEMONTER/EJECTER PYBFLASH, ENLEVER LE CABLE USB DU PC ET LE RACCORDER AU PC.**

Si, malgré ces précautions, PYBFLASH devient inaccessible, vous n'aurez d'autre choix que réinstaller le micologiciel comme indiqué au début de cette page ; vous pourriez même être contraint à réintialiser la mémoire flash du microcontôleur comme évoqué dans la section [FAQ](https://stm32python.gitlab.io/fr/docs/Micropython/faq).<BR>

## Annexe 2 : Vérifier l'attribution du port COM

Un port `COM` a dû être créé sur Windows.

Ecrivez `gestionnaire de périphériques` dans la barre de recherche Windows, puis cliquez sur `Ouvrir` :<br>

![image](images/port_COM.png)

Une nouvelle fenêtre s'ouvre :<br>

![image](images/gestion_peripheriques.png)

Relevez le numéro du port ***COM***. Dans l'exemple ci-dessus, le kit NUCLEO-WB55 est branché sur le port ***COM3***. C'est ce numéro **COMx** qu'il faudra rentrer dans votre terminal série (TeraTerm...).
