---
title: Téléchargement
description: Liens de téléchargement des firmwares et tutoriels, liens vers des ressources sur des sites partenaires

---
# Téléchargements et liens STM32microPython

**Outils software**<br>

[Notepad++](https://drive.google.com/drive/folders/1cvxYGfjHUo5_WrzCS3nMCThETnBiXGI0?usp=sharing)<br>
[TeraTerm](https://drive.google.com/drive/folders/1ZHIoUInsrr_ekHqgWNX5gP6gFnjybX6M?usp=sharing)<br>
[Thonny](https://drive.google.com/drive/folders/1iXiL6h0Fg4It9dGXlwMlYXABr6kiJjk5?usp=sharing)<br>
[PuTTY](https://drive.google.com/drive/folders/1QaN4n6_zORZ7P8XOjuydCYZOuSsGiICq?usp=sharing)<br>
[PyScript](https://drive.google.com/drive/folders/1EI5MaYYN4TEkXPOmChPhXUfrTyALq4LH?usp=sharing)<br>

**Firmware le plus récent**<br>

[Firmware_1.13](https://drive.google.com/drive/folders/1hrllG6rvtkksMqYClUx0DB360AFfaxR5?usp=sharing)

**Scripts Micropython pour les tutoriels de ce site**<br>

- **Tutoriels pour démarrer avec la carte NUCLEO-WB55**<br>
Vous trouverez [ici](https://gitlab.com/stm32python/fr/-/tree/master/assets/Script/start_NUCLEO_WB55/Demarrer_avec_la_carte_NUCLEO_WB55.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Démarrer avec la carte NUCLEO-WB55](exercices).

- **Tutoriels avec la carte X-NUCLEO-IKS01A3**<br>
Vous trouverez [ici](https://gitlab.com/stm32python/fr/-/tree/master/assets/Script/start_XNUCLEO_ISK01A3/Tutoriels_avec_la_carte_XNUCLEO_ISK01A3.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec la carte X-NUCLEO-IKS01A3](iks01a3).

- **Tutoriels avec des modules Grove & autres**<br>
Vous trouverez [ici](https://gitlab.com/stm32python/fr/-/tree/master/assets/Script/start_Grove/Tutoriels_avec_des_modules_Grove_et_autres.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec des modules Grove & autres](grove).

- **Tutoriels avec le BLE**<br>
Vous trouverez *bientôt* ici un fichier ZIP qui rassemblera tous les codes sources présentés dans la section [Tutoriels avec le BLE](ble).


**Autres ressources**<br>

- [**Lampe connectée**](http://icnisnlycee.free.fr/index.php/61-stm32/ble-en-micropython/78-exemple-ble-lampe-connectee)<br>
Piloter une lampe à distance via BLE avec MIT App Inventor. **Merci à Julien Launay !**<br>

- [**Station météo**](https://gitlab.com/stm32python/fr/-/tree/master/assets/projects/station_meteo.zip)<br>
Prototyper une station météo inspirée du produit Ikea Klockis. **Merci à Christophe Priouzeau & Gérald Hallet !**<br>
