---
title: Foire aux Questions
description: Foire aux Questions pour résoudre vos problèmes

---
# Foire aux Questions

<html>
    <ol type="1">
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand une carte n'est subitement plus detectée par Windows ?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Vérifier le câblage.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Essayer un autre port USB.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Changer de câble.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Re-flasher la carte, cf « Guide de démarrage Linux ou Windows10 ».</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand Windows déclare les fichiers comme « corrupt file » ? Qu'il n’est plus possible non plus de placer un fichier sur la carte, d’en réécrire un ou même d’en éditer.</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Ceci arrive dans de rares cas lorsque la carte n’a pas été utilisée « proprement ». Voici quelques conseils :
                    <ol type="i">
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Utiliser Thonny ou Geany (téléchargeable depuis le net).</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Vérifier que le bon port COM est détecté : <i>Tool</i> -> <i>Option</i> -> <i>Interpreter</i> puis se mettre sur <i>Micro Python</i> <i>Port COM xx</i>.</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Ouvrir le fichier depuis l'éditeur de texte. <i>File</i> -> <i>Open</i> puis sélectionner « <i>This Computer</i> » puis utiliser le navigateur pour aller chercher les fichiers sur la carte (PYBFLASH : ).</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Utiliser Ctrl+D pour le soft reboot, n’utiliser le hard reset (bouton reset sur la carte) qu’en cas de non fonctionnement en soft.</li>
                    </ol>
                </li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Il faut re-flasher la carte.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Si même l'étape de re-flash échoue, il faut effacer complètement la mémoire flash du microcontrôleur, avec le programme STM32CubeProgrammer que vous pouvez télécharger sur le site de STMicroelectronics <a href="https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stm32cubeprog.html#get-software" target="_blank">ici</a> (vous devrez vous créer un compte myST) et re-flasher ensuite.</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand les capteurs de la carte d’extension X-NUCLEO IKS01A3 ne renvoient que des « 0 » ?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Tester les branchements.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Eviter de trop enfoncer les connecteurs.</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Que faire quand rien ne se passe avec le shield Grove ?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Vérifier les branchements. Assurez vous notament que toutes les broches sont bien insérées dans les connecteurs Arduino et que le shield Grove n'est pas "décalé" d'une broche par rapport aux connecteurs.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;S’assurer que l’interrupteur en bas à gauche du shield (en dessous du connecteur A0) est bien sur 3.3V ou bien sur 5V si le module branché requiert cette tension d'alimentation.</li>
            </ol>
        </li><br><br>
    </ol>
</html>
