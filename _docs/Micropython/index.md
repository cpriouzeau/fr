---
title: µPython et NUCLEO-WB55
description: µPython et NUCLEO-WB55

---
# MicroPython et NUCLEO-WB55

<p>
<img align="center" src="images/upython.png" alt="upython" width="100"/>
<img align="center" src="images/st2.png" alt="stm" width="250"/>
</p>

Vous trouverez dans cette partie la liste de tous les tutoriels MicroPython développés pour la carte NUCLEO-WB55.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle.
Le protocole à suivre est détaillé dans les 2 guides de démarrage pour Linux et Windows 10.

## Sommaire

* [Qu'est ce que MicroPython ?](micropython)
* [Guide de démarrage rapide Windows 10](install_win10)
* [Guide de démarrage rapide Linux](install_linux)
* [Le kit de développement NUCLEO-WB55](stm32wb55)
* [Démarrer avec la carte NUCLEO-WB55](exercices)
* [Tutoriels avec la carte d'extension X-NUCLEO-IKS01A3](iks01a3)
* [Tutoriels avec des modules Grove et autres](grove)
* [Création d'une application Smartphone avec MIT App Inventor](AppInventor)
* [Capture et enregistrement d’un fichier audio sur carte micro-SD](microphone)
* Programmation d'un mini-robot et contrôle de ses moteurs DC depuis un Smartphone (bientôt)
* [Foire aux Questions](faq)
* [Glossaire](glossaire)
* [Liens Utiles](ressources)
