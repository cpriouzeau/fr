---
title: Glossaire
description: Glossaire

---
# Glossaire

**ADC** : Analog to Digital Converter (fr : Convertisseur Analogique vers Numérique). Circuit électronique spécialisé dans la conversion de signaux d’entrée analogiques (des tensions éventuellement variables dans le temps, comprises entre 0 et +3.3V) en signaux numériques (des nombres entiers proportionnels à la tension d’entrée à chaque instant et compris entre 0 et 4096).

**ALU** : Arithmetic and Logic Unit (fr : Unité d’opérations arithmétiques et logiques). L’unité arithmétique et logique est un circuit électronique intégré au CPU et chargé d'effectuer les calculs sur des nombres entiers et/ou binaires.

**Arduino** : Écosystème éducatif de cartes électroniques développé par la société du même nom.

**BLE** : Bluetooth Low Energy (fr : Bluetooth faible consommation énergétique). C'est une technique de transmission sans fil créée par Nokia en 2006 sous la forme d’un standard ouvert basé sur le Bluetooth, qu'il complète mais sans le remplacer. Comparé au Bluetooth, le BLE permet un débit du même ordre de grandeur (1 Mbit/s) pour une consommation d'énergie 10 fois moindre.

**Bluetooth** : C'est une norme de communication permettant l'échange bidirectionnel de données à courte distance en utilisant des ondes radio UHF sur la bande de fréquence de 2,4 GHz. Son but est de simplifier les connexions entre les appareils électroniques à proximité en supprimant des liaisons filaires. 

**Bootloader** : Système logiciel de démarrage permettant la mise à jour du firmware.

**Cortex M4** : Type de coeur d’un microcontrôleur développé par la société ARM. Microprocesseur offrant un excellent compromis performances / consommation pour des applications embarquées.

**CPU** : Central Processing Unit (fr : Unité Centrale de Traitement). Unité centrale de traitement du microcontrôleur, en l’occurrence un modèle Cortex M4 conçu par la société ARM et optimisé pour être intégré dans les microcontrôleurs et autres SoC.

**DAC** : Digital to Analog Converter (fr : Convertisseur Numérique vers Analogique). Un convertisseur numérique-analogique est un circuit électronique dont la fonction est de transformer une valeur numérique en une valeur analogique qui lui est proportionnelle.

**DFU** : Device Firmware Update (fr: Mise à jour firmware du système).

**Firmware** : Nom donné au fichier binaire qui contient un programme compilé destiné à un microcontrôleur.

**Flash (mémoire)** : Type de mémoire non volatile dans laquelle est placé le code compilé. Mémoire qui ne s’efface pas lorsque son alimentation électrique est éteinte. Elle est approximativement quatre fois plus dense, càd qu’elle peut stocker plus de bits par unité de surface, que la RAM. En contrepartie, la mémoire flash est plus lente à lire et à écrie que la RAM.

**GPIO** : General Purpose Input Output (fr : Port D’entrée Sortie Généraliste). Les Ports d’entrée-sortie à usage général désignent l’ensemble des fonctions (programmables) que peuvent remplir les broches du microcontrôleur. Ils permettent de sélectionner les circuits internes au microcontrôleur que l’on souhaite utiliser afin d’interagir avec des composants externes et connectés à celui-ci tels que mémoires, moteurs, capteurs…

**I2C** : Inter-Integrated Circuit (fr : _bus de communication_ intégré inter-circuits). Contrôleur de bus série fonctionnant selon un protocole inventé par Philips. Tout comme le bus SPI, le bus I2C a été créé pour fournir un moyen simple de transférer des informations numériques entre des capteurs, des actuateurs, des afficheurs et un microcontrôleur.

**LCD** : Liquid Crystals Display (fr: afficheur à cristaux liquides).

**LED** : Luminescent Electronic Diode (fr: Diode ÉlectroLuminescente).

**LPUART** : *Low Power* Universal Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Asynchrone Universel *à basse consommation*). Voir *UART*.

**MCU** ou **µC** : Microcontroler Unit (fr : Unité Microcontrôleur). Un microcontrôleur est un « système sur puce » (SoC) qui rassemble sur une même puce de silicium un microprocesseur et d’autres composants et périphériques : des bus, des horloges, de la mémoire, des ports entrées-sorties généralistes, etc

**MORPHO** : Type de connecteur propriétaire STMicroelectronics sur les cartes NUCLEO, donnant accès à l'ensemble des broches du microcontrôleur.

**NUCLEO** : Nom commercial donné aux cartes de développement fabriquées par STMicroelectronics.

**OLED** : Organic Luminescent Electronic Diode (fr: Diode ÉlectroLuminescente Organique).

**One-Wire** (fr : "Un câble"): Aussi connu sous le nom de bus Dallas ou 1-Wire, c'est un bus conçu par Dallas Semiconductor qui permet de connecter (en série, parallèle ou en étoile) des composants avec seulement deux fils (un fil de données et un fil de masse).

**PWM** : Pulse Width Modulation (fr : Modulation en Largeur d’Impulsion). La modulation en largeur d’impulsion est une technique permettant de faire varier le potentiel électrique d’une broche entre 0 et +3.3V (pour le microcontrôleur concerné) selon un signal rectangulaire de fréquence et de rapport cyclique (ratio entre le temps où le signal est à 0V et celui où il est à +3.3V au sein d’une période) dynamiquement ajustables. La génération de signaux PWM est l’une des fonctions assurées par les timers, essentiellement pour piloter des moteurs électriques.

**RAM (mémoire)** : Type de mémoire volatile dans laquelle sont placées les variables. La mémoire à accès aléatoire ou mémoire vive est rapide et peut-être adressée bit par bit. Elle est nettement plus performante que la mémoire flash mais est disponible en quantité plus réduite du fait de son architecture plus complexe et moins dense. Toutes les informations enregistrées dans une mémoire RAM sont perdues lorsque son alimentation électrique est interrompue.

**RFID** : Radio Frequency Identification (fr : radio-identification). La radio-identification est une méthode pour mémoriser et récupérer des données à distance en utilisant des marqueurs appelés « radio-étiquettes » (« RFID tag » ou « RFID transponder » en anglais).

**ROM (mémoire)** : Read Only Memory (fr : mémoire en lecture seule). Mémoire qui peut être extrêmement dense car potentiellement encodée dans la structure même du microcontrôleur lors de sa fabrication. Les instructions exécutées par le CPU sont par exemple répertoriées dans une ROM.

**RTC** : Real-Time Clock (fr : Horloge Temps-Réel). Circuit électronique remplissant les fonctions d’une horloge très précise pour des problématiques d’horodatage, de gestions d’alarmes déclenchées suivant un calendrier et capable de mettre le microcontrôleur en sommeil ou de le réveiller, etc.

**SoC** : System on Chip (fr : Système sur Puce). Désigne un système complet embarqué sur une seule puce (« circuit intégré ») et pouvant comprendre de la mémoire, un ou plusieurs microprocesseurs, des périphériques d'interface ou tout autre composant nécessaire à la réalisation d’une fonction requise. Les microcontrôleurs sont des SoC. 

**SPI** : Serial Peripheral Interface (fr: Interface Série pour Périphériques). Contrôleur de bus série fonctionnant selon un protocole inventé par Motorola. Un bus série de ce type permet la connexion, de type maître-esclave, de plusieurs circuits disposant d’interfaces compatibles avec trois fils de liaisons. 

**ST-LINK V2** : Programmeur fabriqué par STMicroelectronics pour transférer le code dans les mémoires FLASH et RAM d’un microcontrôleur.

**TIM** : Timer (fr : Compteur). Circuit électronique basé sur un compteur qui coordonne des fonctions variées (selon son architecture) telles que lecture de signaux extérieurs variables dans le temps, génération de signaux modulés en largeur d’impulsion (PWM & contrôle de moteurs électriques), génération d’évènements périodiques, etc.

**UART** : Universal Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Asynchrone Universel). En langage courant, c'est un composant utilisé pour faire la liaison entre le microcontrôleur et l’un de ses ports série.

**USART** : Universal Synchronous/Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Synchrone/Asynchrone Universel). Voir *UART*.


