---
title: Exercice avec la carte d'extension IKS01A1 en C/C++ pour Stm32duino
description: Exercice avec la carte d'extension IKS01A1 en C/C++ pour Stm32duino
---
# Exercice avec la carte d'extension IKS01A1 en C/C++ pour Stm32duino

Vous devrez disposez de la [carte d’extension IKS01A1](https://www.st.com/en/ecosystems/x-nucleo-iks01a1.html) pour continuer ces exercices.

La [carte d’extension IKS01A1](https://www.st.com/en/ecosystems/x-nucleo-iks01a1.html) est l'ancienne version de la [carte d’extension IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)  

## Démarrage

La carte d’extension IKS01A1 est une carte de démonstration de plusieurs capteurs MEMS de ST Microelectronics. Sa version A3 contient les capteurs suivants: 

* LSM6DS0:  acceleromètre 3D (±2/±4/±8 g) + gyroscope 3D (±245/±500/±2000 dps)
* LIS3MDL: magnetomètre 3D (±4/ ±8/ ±12/ 16 gauss)
* LPS25HB: baromètre 260-1260 hPa
* HTS221: humidité et température


Ces capteurs sont raccordés au bus I2C de la carte P-NUCLEO-WB55.

La carte d’extension IKS01A1 dispose également d'un emplacement au format DIL 24 broches pour y ajouter des capteurs I2C supplémentaires (par exemple, le gyroscope [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)) 

![image](images/iks01a1.png)

## Branchement

Brancher la carte IKS01A1, attention à bien respecter le marquage des connecteur : CN9 -> CN9, CN5 -> CN5, etc...

Une fois que vous aurez raccordé correctement la carte d’extension, voici la liste des capteurs avec lesquels vous pouvez dialoguer par I2C.

## Installation des bibliothèques pour la carte d'extension IKS01A3

### Sur Windows
A faire

### Sur Linux
A faire

### Sur MacOS
Entrez les commandes suivantes :
```bash
cd ~/Documents/Arduino/libraries/
for lib in X-NUCLEO-IKS01A1 HTS221 LPS25HB LSM6DS0 LIS3MDL
do
wget https://codeload.github.com/stm32duino/${lib}/zip/master -O ${lib}-master.zip
unzip ${lib}-master.zip
done 
```

## Utilisation

Lancez l'IDE Arduino (préalablement configuré pour Stm32duino)

Ouvrez le croquis d'exemple `Fichier > Exemples > STM32Duino X-NUCLEO-IKS01A1 > X_NUCLEO_IKS01A1_HelloWorld`.

Branchez la carte Nucleo muni de la carte IKS01A3.

Compilez et chargez le binaire produit.

Ouvrez la console série qui affiche les traces suivantes :
```
| Hum[%]: 68.00 | Temp[C]: 20.80 | Pres[hPa]: 992.06 | Temp2[C]: 17.30 | Acc[mg]: -96 86 1011 | Gyr[mdps]: 910 -1260 1260 | Mag[mGauss]: -20 2 63 |
| Hum[%]: 67.80 | Temp[C]: 20.80 | Pres[hPa]: 992.03 | Temp2[C]: 17.30 | Acc[mg]: -94 84 1012 | Gyr[mdps]: 700 -1050 1540 | Mag[mGauss]: -17 2 56 |
| Hum[%]: 67.80 | Temp[C]: 20.80 | Pres[hPa]: 992.10 | Temp2[C]: 17.40 | Acc[mg]: -96 87 1014 | Gyr[mdps]: 980 -1120 1260 | Mag[mGauss]: -21 4 62 |
| Hum[%]: 67.60 | Temp[C]: 20.80 | Pres[hPa]: 992.03 | Temp2[C]: 17.40 | Acc[mg]: -141 89 1229 | Gyr[mdps]: 23310 91000 -76440 | Mag[mGauss]: 4 -4 78 |
| Hum[%]: 68.00 | Temp[C]: 20.80 | Pres[hPa]: 991.98 | Temp2[C]: 17.40 | Acc[mg]: 411 641 -147 | Gyr[mdps]: 76440 -271110 100940 | Mag[mGauss]: 212 -259 201 |
| Hum[%]: 67.90 | Temp[C]: 20.80 | Pres[hPa]: 992.14 | Temp2[C]: 17.40 | Acc[mg]: 511 409 -833 | Gyr[mdps]: 11970 -18060 1960 | Mag[mGauss]: 104 -328 788 |
| Hum[%]: 67.90 | Temp[C]: 20.80 | Pres[hPa]: 992.08 | Temp2[C]: 17.40 | Acc[mg]: 568 662 -499 | Gyr[mdps]: 10640 177660 -256060 | Mag[mGauss]: 341 -412 534 |
| Hum[%]: 67.90 | Temp[C]: 20.80 | Pres[hPa]: 992.17 | Temp2[C]: 17.40 | Acc[mg]: -159 988 28 | Gyr[mdps]: 1330 1050 7000 | Mag[mGauss]: 474 -164 386 |
| Hum[%]: 68.30 | Temp[C]: 20.80 | Pres[hPa]: 992.23 | Temp2[C]: 17.40 | Acc[mg]: -271 983 179 | Gyr[mdps]: 13440 -3080 5320 | Mag[mGauss]: 471 -163 373 |
| Hum[%]: 67.80 | Temp[C]: 20.80 | Pres[hPa]: 992.05 | Temp2[C]: 17.50 | Acc[mg]: 390 -1069 156 | Gyr[mdps]: 38220 27720 24850 | Mag[mGauss]: -192 -330 416 |
| Hum[%]: 68.00 | Temp[C]: 20.80 | Pres[hPa]: 992.15 | Temp2[C]: 17.50 | Acc[mg]: 68 296 1162 | Gyr[mdps]: -9520 22750 118580 | Mag[mGauss]: 95 -103 91 |
| Hum[%]: 67.60 | Temp[C]: 20.80 | Pres[hPa]: 992.12 | Temp2[C]: 17.50 | Acc[mg]: 420 21 1003 | Gyr[mdps]: 3990 -4200 6300 | Mag[mGauss]: 4 31 69 |
| Hum[%]: 67.60 | Temp[C]: 20.80 | Pres[hPa]: 992.19 | Temp2[C]: 17.50 | Acc[mg]: 39 153 1008 | Gyr[mdps]: 840 -1120 1260 | Mag[mGauss]: 138 -20 93 |
| Hum[%]: 67.40 | Temp[C]: 20.80 | Pres[hPa]: 992.18 | Temp2[C]: 17.50 | Acc[mg]: 41 151 1010 | Gyr[mdps]: 910 -1120 1330 | Mag[mGauss]: 140 -22 92 |
| Hum[%]: 67.20 | Temp[C]: 20.80 | Pres[hPa]: 992.04 | Temp2[C]: 17.50 | Acc[mg]: 42 151 1007 | Gyr[mdps]: -630 -1190 1400 | Mag[mGauss]: 131 -2 91 |

```

Bougez la carte pour faire varier les valeurs des capteurs Acc, Gyr, Mag.






