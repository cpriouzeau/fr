---
title: Exercice avec le capteur de luminosité Grove en C/C++ pour Stm32duino
description: Exercice avec le capteur de luminosité Grove en C/C++ pour Stm32duino
---
# Exercice avec le capteur de luminosité Grove en C/C++ pour Stm32duino


**Cf tuto capteur tiltsensor pour les prérequis**


- **Capteur de luminosité :**

![Image](images/6_luminosite/capteur-de-lumiere.png)

Ce capteur utilise une photorésistance afin de détecter l'intensité lumineuse de son environnement. La valeur de cette résistance diminue lorsque qu'elle est éclairée.


- *Voici le code sur Arduino*

```c
void setup() {
  Serial.begin(9600);
  pinMode(D4,INPUT);

}
void loop() {
  Serial.println(digitalRead(D4));
  delay(500);
}
```

Vérifiez et téléversez.  
Pour regarder l'état dans lequel se trouve le capteur cliquez sur le *moniteur série*.

![Image](images/6_luminosite/moniteur_serie.png)

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
