---
title: Jupyter
description: Comment utiliser le kit STM32 MicroPython avec Jupyter
---
# Comment utiliser le kit STM32 MicroPython avec Jupyter ?

## Jupyter
[Jupyter](https://jupyter.org/) est une application qui permet de programmer dans plusieurs langages de programmation comme Python, Julia, Ruby, R, ou encore Scala. Jupyter permet de réaliser des calepins ou notebooks mélant des notes au format Markdown, des programmes ainsi que les résultats de leurs exécutions.

## Sommaire

Vous trouverez dans cette partie le tutoriel vous permettant de créer un Notebook [Jupyter](https://jupyter.org/) pour traiter et afficher en temps réel les mesures relevées par les capteurs branchés sur la carte STM32.

* [Installation de Jupyter](install)
* [Utilisation d'un notebook Jupyter](notebook)
